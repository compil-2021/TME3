;;;; Exercice 1 : Euclide

#lang racket

;;; Q1

(define (pgcd a b)
  (define t b)
  ;; Il n'y a pas de construction "while"
  ;; en scheme, donc on définit une fonction
  ;; "while" à la place
  (define (while)
    ;; On teste la condition du while
    (when (not (zero? b))
      ;; Si la condition est vrai on mets à
      ;; jour les variables
      ;; t := b
      ;; b := (modulo a b)
      ;; a := t
      (begin
        (set! t b)
        (set! b (modulo a b))
        (set! a t)
        ;; Et on refait un tour de boucle
        (while))))
  ;; Comme le while est une fonction, il
  ;; faut lançer la boucle explicitement
  (while)
  a)

;;; Q2

(define (pgcd2 a b)
  (if (zero? b) a
      (pgcd2 b (modulo a b))))
